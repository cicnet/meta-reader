import { toAddressKey } from '@cicnet/crdt-meta'

import { TokenContract, Web3Instance } from '../src/index.js'

// Need to be running cic-internal-integration locally
const web3Provider = 'ws://localhost:63546'
const web3Instance = Web3Instance(web3Provider)
const tokenContractAddress = '0xb708175e3f6Cd850643aAF7B32212AFad50e2549'
const tokenContract = TokenContract(tokenContractAddress, web3Instance)

async function getBalance(address) {
  return await tokenContract.methods.balanceOf(address).call()
}

async function getAddressMetaData(address, identifier) {
  return await toAddressKey(address, identifier)
}

try {
  const blockchainAddress = '0x51899362a273dba03f48bCd0E0423984B53c1619'
  const balance = await getBalance(blockchainAddress)
  console.log(`${blockchainAddress} => ${balance}`)

  const custom = await getAddressMetaData(blockchainAddress, ':cic.person')
  console.log(custom)
} finally {
  web3Instance.currentProvider.connection.close()
}
