import test from 'tape'

import {
  getAccountsAddresses,
  getAccountsTotal,
  getAccountDataFromMeta,
  getAccountKeyFromAddress,
  getAddressFromRegistry,
  getTokenBalance,
  getUserData,
  parseVcardData,
  TokenContract,
  Web3Instance
} from '../src/index.js'

// Need to be running cic-internal-integration locally
const metaServer = 'http://localhost:63380'
const web3Provider = 'ws://localhost:63546'
const web3Instance = Web3Instance(web3Provider)
const contractRegistryAddress = '0xea6225212005e86a4490018ded4bf37f3e772161'
const tokenContractAddress = '0xb708175e3f6Cd850643aAF7B32212AFad50e2549'
const tokenContract = TokenContract(tokenContractAddress, web3Instance)

let address, accountIndexAddress

test('Go through flow of getting to the Account Index and pulling through data', function (t) {
  let accountData, accountKey

  t.test(
    'Get Account Index address - getAddressFromRegistry',
    async function (t) {
      accountIndexAddress = await getAddressFromRegistry(
        'AccountRegistry',
        contractRegistryAddress,
        web3Instance
      )

      t.equal(
        accountIndexAddress,
        '0xd0097a901AF4ac2E63A5b6E86be8Ad91f10b05d7',
        'Should return correct Account Index address'
      )
    }
  )

  t.test('Get total number of accounts - getAccountsTotal', async function (t) {
    const accountsTotal = await getAccountsTotal(
      accountIndexAddress,
      web3Instance
    )

    t.equal(
      parseInt(accountsTotal, 10),
      50,
      'Should return correct total number of accounts'
    )
  })

  t.test(
    'Get all account addresses - getAccountsAddresses',
    async function (t) {
      const accountsAddresses = await getAccountsAddresses(
        accountIndexAddress,
        web3Instance
      )
      address = accountsAddresses[1]

      // console.log('>>>>> address', address)
      // console.log('>>>>> accountsAddresses', accountsAddresses)
      t.ok(accountsAddresses, 'should return an array of account addresses')
    }
  )

  t.test(
    'Get an account key from an address - getAccountKeyFromAddress',
    async function (t) {
      accountKey = await getAccountKeyFromAddress(address)

      // console.log('>>>>> accountKey', accountKey)
      t.equal(
        typeof accountKey,
        'string',
        'Should return correct account key from address'
      )
    }
  )

  t.test(
    'Retrieve account user data from meta server - getAccountDataFromMeta',
    async function (t) {
      accountData = await getAccountDataFromMeta(metaServer, accountKey)

      // console.log('>>>>> accountData', JSON.stringify(accountData, 0, 2))
      t.equal(
        typeof accountData,
        'object',
        'should return an account data object'
      )
    }
  )

  t.test('Parse Vcard data', async function (t) {
    const vcardData = parseVcardData(accountData.vcard)

    // console.log('>>>>> vcardData', JSON.stringify(vcardData, 0, 2))
    t.equal(typeof vcardData, 'object', 'should return Vcard data object')
  })
})

test('Fetch the user data - getUserData', async function (t) {
  const userData = await getUserData(metaServer, address)

  // console.log('>>>>> userData', JSON.stringify(userData, 0, 2))
  t.equal(typeof userData, 'object', 'should return an account data object')
})

test('Get a user balance - getTokenBalance', async function (t) {
  const addressBalance = await getTokenBalance(address, tokenContract)

  // console.log('>>>>> addressBalance', addressBalance, parseInt(addressBalance))
  t.equal(
    typeof addressBalance,
    'string',
    'should return a balance as a string'
  )

  t.teardown(() => web3Instance.currentProvider.connection.close())
})
