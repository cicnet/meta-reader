# meta-reader

## Getting Started

```javascript
import { getUserData } from '@cicnet/meta-reader'

const metaServer = 'http://localhost:63380'
const address = '0xbE7670D37425689d7289e177aE52f05C88C3Cd50'
const userData = await getUserData(metaServer, address)

console.log('--- userData ---\n', JSON.stringify(userData))

/*
ᐅ node index.js
--- userData ---
 {"date_registered":1530283787,"gender":"female","identities":{"evm":{"oldchain:1":["0xd3Ec6Ab301a7F9084b1dA60899cd6A663452dfC5"],"bloxberg:8996":["0xbE7670D37425689d7289e177aE52f05C88C3Cd50"]},"latitude":90.90422105671476,"longitude":180.33459577742474},"location":{"area_name":"Zagorje ob Savi"},"products":[],"vcard":{"version":[{"value":"3.0"}],"email":[{"value":"tommikkelsen@yahoo.com"}],"fn":[{"value":"Vladimir, Smith"}],"n":[{"value":["Smith","Vladimir","","",""]}],"tel":[{"value":"+254815065188","meta":{"typ":["CELL"]}}]}}
 */
```

## Testing

1. Checkout `master` in `grassrootseconomics/cic-internal-integration`
2. Startup CIC Internal Integration Docker Compose environment and run scripts to create users and balances
3. Run `npm run watch`
