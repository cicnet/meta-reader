import * as registryJson from './abi/RegistryJSON.js'

export const Registry = (contractAddress, web3Instance) => {
  const contract = new web3Instance.eth.Contract(
    registryJson.default,
    contractAddress
  )

  return {
    addressOf: async function (identifier) {
      const id =
        '0x' +
        web3Instance.utils.padRight(
          new Buffer.from(identifier).toString('hex'),
          64
        )
      return await contract.methods.addressOf(id).call()
    }
  }
}
