import * as accountIndexJson from './abi/AccountIndexJSON.js'

export const AccountIndex = (contractAddress, web3Instance) => {
  const contract = new web3Instance.eth.Contract(
    accountIndexJson.default,
    contractAddress
  )

  return {
    totalAccounts: async function () {
      return await contract.methods.entryCount().call()
    },

    last: async function (numberOfAccounts) {
      const count = await this.totalAccounts()
      let lowest = count - numberOfAccounts - 1
      if (lowest < 0) {
        lowest = 0
      }
      let accounts = []
      for (let i = count - 1; i >= lowest; i--) {
        const account = await contract.methods.entry(i).call()
        accounts.push(account)
      }
      return accounts
    }
  }
}
