import atob from 'atob'
import Web3 from 'web3'
import { parse } from 'vcard-parser'
import { toAddressKey } from '@cicnet/crdt-meta'

import fetcher from '../utils/fetcher.js'
import { AccountIndex, Registry } from '../lib/eth/index.js'
import * as GiftableTokenJson from '../lib/eth/abi/GiftableTokenJSON.js'

export async function getAccountsAddresses(accountIndexAddress, web3Instance) {
  const accountIndexQuery = AccountIndex(accountIndexAddress, web3Instance)
  return await accountIndexQuery.last(await accountIndexQuery.totalAccounts())
}

export async function getAccountsTotal(accountIndexAddress, web3Instance) {
  const accountIndexQuery = AccountIndex(accountIndexAddress, web3Instance)
  return await accountIndexQuery.totalAccounts()
}

export async function getAccountDataFromMeta(metaServer, accountKey) {
  return await fetcher(`${metaServer}/${accountKey}`)
}

export async function getAccountKeyFromAddress(blockchainAddress) {
  return await toAddressKey(blockchainAddress, ':cic.person')
}

export async function getAddressFromRegistry(
  registryName,
  contractRegistryAddress,
  web3Instance
) {
  const registry = Registry(contractRegistryAddress, web3Instance)
  return await registry.addressOf(registryName, web3Instance)
}

export async function getTokenBalance(blockchainAddress, tokenContract) {
  return await tokenContract.methods.balanceOf(blockchainAddress).call()
}

export async function getUserData(metaServer, blockchainAddress) {
  const accountKey = await getAccountKeyFromAddress(blockchainAddress)
  const accountData = await getAccountDataFromMeta(metaServer, accountKey)
  const vcardData = parseVcardData(accountData.vcard)
  accountData.vcard = vcardData
  return accountData
}

export function parseVcardData(vcardData) {
  return parse(atob(vcardData))
}

export const Web3Instance = web3Provider => new Web3(web3Provider)

export const TokenContract = (tokenContractAddress, web3Instance) =>
  new web3Instance.eth.Contract(GiftableTokenJson.default, tokenContractAddress)
